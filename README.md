# Assessment Prototype

## Summary

The prototype source has four elements:

* prototype.py - Python 3 program which will run on the data
* papers.tar.gz - GZIP TAR file with PDF papers and Spreadsheet of files to titles
* requirements.txt - Python requirements file for installation of libraries
* README.md - this file

## Run Prototype

* Extract contents of papers.zip to the source directory - unarchiving Papers/ and index.xlsx
* Create directories Docs, Ents, JSON, data in the source directory
* Create a Virtual Environment for Python
* Activate the Virtual Environemnt and install the libraries in requirements.txt
* Run the program: python prototype.py
